<?php
namespace App\Repository;

use App\Models\ChatAppGroup;
class ChatAppRepository
{
    private $chatAppGroup;
    public function __construct(ChatAppGroup $chatAppGroup)
    {
        $this->chatAppGroup = $chatAppGroup;
    }
    public function testing(){
        return $this->chatAppGroup->create(['group_name'=>'Malkhan Group']);
    }
    public function createGroup($groupName){
        return $this->chatAppGroup->create(['group_name'=>$groupName]);
    }

}
