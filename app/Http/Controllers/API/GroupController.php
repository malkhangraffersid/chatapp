<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ChatAppGroup;
use App\Models\Participant;
use App\Models\Message;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $chatGroup, $participant, $message;
    public function __construct(ChatAppGroup $chatGroup, Participant $participant, Message $message){
        $this->chatGroup = $chatGroup;
        $this->participant = $participant;
        $this->message = $message;
    }
    public function index()
    {
        $loggedInUser = \Auth::user();
        $userID = $loggedInUser->id;
        $data = $this->chatGroup->select('chatApp_groups.id','chatApp_groups.group_name')
                    ->join('chatApp_participants as part','chatApp_groups.id','=','part.group_id')->where('part.user_id',$userID)->get();
        return ['status'=>true,'message'=>'User Group Listing','groups'=>$data];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $loggedInUserID = \Auth::user()->id;
        $group =  new ChatAppGroup();
        $group->group_name = $request->group_name;
        $group->save();
        
        $participants = array('participants'=>$request->participants,'group_id'=>$group->id);
        foreach($request->participants as $ParticipantID):
            if($loggedInUserID==$ParticipantID){
                $userRole = 'Admin';
            }else{
                $userRole = 'User';
            }
            $data = array("group_id"=>$group->id,'user_id'=>$ParticipantID,'role'=>$userRole);
            $this->participant->insert($data);
        endforeach;
        //$group = $this->chatGroup->insert($data);
        return ['status'=>true,'message'=>'User Group Created','group'=>$participants];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $messages = $this->message->select('id','user_id','message','created_at')->where('group_id',$id)->get();
        $participants = $this->participant->select('id','user_id','role','created_at')->where('group_id',$id)->get();
        //$participants = $this->participant->where('group_id',$id)->get();
        return ['status'=>true,'message'=>'Participants List','participants'=>$participants,'messages'=>$messages];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
