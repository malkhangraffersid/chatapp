<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatAppGroup extends Model
{
    protected $fillable = ['group_name'];
    protected $table = "chatApp_groups";
}
