<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChatAppMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatApp_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_id')->index("messagesGroup_id_foreign");
            $table->unsignedInteger('user_id')->index("messagesUser_id_foreign");
            $table->string('message');
            $table->timestamps();
            $table->foreign('group_id')->references('id')->on('chatApp_groups');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatApp_messages');
    }
}
