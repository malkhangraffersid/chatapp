<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChatAppParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatApp_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_id')->index("participantsGroup_id_foreign");
            $table->unsignedInteger('user_id')->index("participantsUser_id_foreign");
            $table->string('role');
            $table->timestamps();
            $table->foreign('group_id')->references('id')->on('chatApp_groups');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatApp_participants');
    }
}
